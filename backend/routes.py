import os

from PIL import Image
from flask_cors import cross_origin
from flask import request, render_template, redirect, make_response, Blueprint
from sahi import AutoDetectionModel
from sahi.predict import get_sliced_prediction
from werkzeug.utils import secure_filename

from edamam.api import get_info_from_db, get_available_classes_from_db
from modules import handle_labels

detection_model = AutoDetectionModel.from_pretrained(
    model_type='yolov8',
    model_path='yolov8s.pt',
    confidence_threshold=0.55,
    device='cuda:0',  # or 'cpu',
)


def set_routes(app):
    api_blueprint = Blueprint('api', __name__, url_prefix='/api')
    @api_blueprint.route('/classes', methods=['GET'])
    @cross_origin(supports_credentials=True)
    def classes():
        if request.method == 'GET':
            return get_available_classes_from_db()

    @api_blueprint.route('/info', methods=['POST'])
    @cross_origin(supports_credentials=True)
    def info():
        data = request.json
        if data.get('labels'):
            return get_info_from_db(data.get('labels'))
        return True

    @api_blueprint.route('/analyze', methods=['POST'])
    @cross_origin(supports_credentials=True)
    def analyze():
        if request.method == 'POST':
            print(str(request.files))
            f = request.files.get('file')
            if not f:
                return {"message": "no file"}

            path = os.path.join('upload/', secure_filename(f.filename))
            f.save(path)

            img = Image.open(path)
            aspect = int(img.height / 2)

            result = get_sliced_prediction(
                path,
                detection_model,
                slice_height=aspect,
                slice_width=aspect,
                overlap_height_ratio=0.2,
                overlap_width_ratio=0.2
            )
            result.export_visuals(export_dir="data/", file_name='last')

            labels = handle_labels(result)

            got_labels = get_info_from_db(labels)
            return got_labels

    @api_blueprint.after_request
    def add_header(response):
        # Include cookie for every request
        response.headers.add('Access-Control-Allow-Credentials', True)

        # Prevent the client from caching the response
        if 'Cache-Control' not in response.headers:
            response.headers[
                'Cache-Control'] = 'public, no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
            response.headers['Pragma'] = 'no-cache'
            response.headers['Expires'] = '-1'
        return response

    app.register_blueprint(api_blueprint)
