import {useCallback, useEffect, useState} from 'react'
import './App.css'
import axios from "axios";
import {useDropzone} from 'react-dropzone';
import Entry, {EntryProps} from "./components/Entry";
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

function App() {
    const [preview, setPreview] = useState<string | ArrayBuffer | null>(null);
    const [food, setFood] = useState<EntryProps[]>([]);
    const [variants, setVariants] = useState<string[]>([]);

    useEffect(() => {
        axios
          .get("http://localhost:3009/api/classes", {})
          .then((res) => {
              setVariants(filterUnique(res.data))
          })
    }, [])

    const selectVariant = (v) => {
        console.log(v.label)
        let kfood = Object.keys(food).map(v => v.toLowerCase())
        kfood.push(v.label)
        kfood = filterUnique(kfood)
        axios
          .post("http://localhost:3009/api/info", { labels: kfood }, {
              headers: {
                  'Content-Type': 'application/json'
              }
          })
          .then((res) => {
              setFood(res.data)
          })
    }

    const onDrop = useCallback((acceptedFiles: Array<File>) => {
        const file = new FileReader;

        file.onload = function() {
            setPreview(file.result);
        }
        file.readAsDataURL(acceptedFiles[0])

        if ( typeof acceptedFiles[0] === 'undefined' ) return;

        const formData = new FormData();
        formData.append('file', acceptedFiles[0]);
        axios
          .post("http://localhost:3009/api/analyze", formData, {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          })
          .then((res) => {
            console.log(res);
            setFood(res.data);
          })
    }, [])

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
      onDrop,
    });

    return (
        <>
            <div {...getRootProps()}>
              <input {...getInputProps()} />
              {
                isDragActive ?
                  <p>Отпустите файл здесь...</p> :
                  <p>Перетащите файл сюда или нажмите чтобы выбрать</p>
              }
            </div>
            {preview && (
              <p><img height={"500px"} src={preview as string} alt="Upload preview" /></p>
            )}
            <div style={{ display: "flex", justifyContent: "center", flexDirection: "column" }}>
                {
                    Object.keys(food).map(f => (
                        <Entry setFood={setFood} name={f} protein={food[f].protein} calories={food[f].calories} carbs={food[f].carbs} fat={food[f].fat} fiber={food[f].fiber}/>
                    ))
                }
            </div>
            {
                preview
                ? <Dropdown options={variants} onChange={selectVariant} value={0} placeholder="Select an option" />
                : ""
            }
        </>
    )
}

export default App


function filterUnique(arr) {
    return arr.filter(function (v, i, self) {
        return i == self.indexOf(v);
    })
}