import {FC} from 'react'

export interface EntryProps {
    name: string,
    calories: number,
    carbs: number,
    fat: number,
    fiber: number,
    protein: number
    setFood: any
}

const Entry = (props: EntryProps): FC => {

  const onDelete = (name) => {
      props.setFood((prevState) => {
        const updatedFood = { ...prevState };
        delete updatedFood[name];
        return updatedFood;
      })
  }
  return (
      <div style={{ display: "flex", justifyContent: "center", flexDirection: "row" }}>
          <div key={ props.name } className={"foodCard"}>
              <p onClick={() => onDelete(props.name)} className={"foodItem"} style={{"background": "#ffd1d1"}}>{ props.name } (x)</p>
              <p className={"foodItem"}>Calories: { props.calories.toFixed(1) }</p>
              <p className={"foodItem"}>Carbs: { props.carbs.toFixed(1) }</p>
              <p className={"foodItem"}>Fat: { props.fat.toFixed(1) }</p>
              <p className={"foodItem"}>Fiber: { props.fiber.toFixed(1) }</p>
              <p className={"foodItem"}>Protein: { props.protein.toFixed(1) }</p>
          </div>

      </div>
  ) as FC;
};

export default Entry;