import os

from flask import Flask
from flask_cors import CORS
from backend.routes import set_routes

app = Flask(__name__, static_folder='static')
CORS(app, resources={r"/api/*": {"origins": "*"}})
set_routes(app)

if __name__ == '__main__':
    if not os.path.exists('upload'):
        os.makedirs('upload', exist_ok=True)

    app.run(host='0.0.0.0', port=3009, debug=True, use_reloader=False)

