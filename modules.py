from edamam.api import get_info_from_db


def handle_labels(results):
    labels = []
    for res in results.object_prediction_list:
        food_name = res.category.name.replace('-', ' ')
        labels.append(food_name)
    return labels
